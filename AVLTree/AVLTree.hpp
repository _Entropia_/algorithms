#include <cinttypes>
#include <iostream>
#include <memory>

#include "Tree/TreeNode.hpp"
#include "Tree/TreeIterator.hpp"

template <typename T>
class AVLTree
{
public:
  // @todo(ratsch) does it need to be public?
  class Node : public TreeNode<T, AVLTree<T>::Node>
  {
  public:
    using TreeNode<T, Node>::TreeNode;
    int8_t balance = 0;
    
    std::string GetLabel() const;
  };

  using iterator = TreeIterator<Node>;

  AVLTree();
  ~AVLTree();

  iterator begin() const;
  iterator end() const;
  
  // @todo(ratsch) add inorder iterator, return that
  bool Contains( const T &v ) const;

  void Insert( const typename Node::value_type &v );

private:
  std::unique_ptr<Node> root_;
};

