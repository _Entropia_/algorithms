#include <cassert>
#include <cmath>
#include <iostream>
#include <functional>
#include <memory>
#include <utility>
#include <stdexcept>
#include <vector>

#include "AVLTree/AVLTree.hpp"
#include "Tree/TreeNode.hxx"
#include "Tree/TreeIterator.hxx"
#include "Tree/util.hpp"

namespace
{

template <typename T>
int64_t check_height( const typename AVLTree<T>::Node *n, int64_t current_height )
{
  if ( !n )
    return current_height;

  int64_t left_height = check_height<T>( n->GetLeft().get(), current_height + 1 );
  int64_t right_height = check_height<T>( n->GetRight().get(), current_height + 1 );

  int64_t balance_factor = right_height - left_height;
  if ( balance_factor != n->balance )
    throw std::runtime_error( "inconsistent balance factor" );
  if ( std::abs( balance_factor ) > 1 )
    throw std::runtime_error( "invalid height" );

  return std::max( left_height, right_height );
};
  
template <typename T>
bool is_consistent( const typename AVLTree<T>::Node &root )
{
  std::function<bool(const typename AVLTree<T>::Node &)> check_sorted;
  check_sorted = [&check_sorted]( const typename  AVLTree<T>::Node &n )
  {
    if ( n.GetLeft() && n.GetLeft()->GetValue() > n.GetValue() )
      return false;
    if ( n.GetRight() && n.GetRight()->GetValue() <= n.GetValue() )
      return false;
    return true;
  };

  try
  {
    check_height<T>( &root, 0 );
  }
  catch ( const std::runtime_error & )
  {
    return false;
  }
  return check_sorted( root );
}
  
template <typename T>
void rotate_balanced( std::unique_ptr<typename AVLTree<T>::Node> &node_ptr,
                      std::unique_ptr<typename AVLTree<T>::Node> &parent_ptr )
{
  auto node_balance = node_ptr->balance;
  auto parent_balance = parent_ptr->balance;
  
  bool is_clockwise = node_ptr == parent_ptr->GetLeft();
  assert( is_clockwise || node_ptr == parent_ptr->GetRight() );
  
  // "outer rotation": clockwise and node_balance < 0 or
  //                   counter-clockwise and node_balance >= 0
  bool is_outer = is_clockwise == ( node_balance < 0 );
  
  int8_t new_node_balance;
  int8_t new_parent_balance;
  if ( is_outer )
  {
    if ( is_clockwise )
    {
      new_node_balance = parent_balance + 2;
      new_parent_balance = parent_balance - node_balance + 1;
    }
    else
    {
      new_node_balance = parent_balance - 2;
      new_parent_balance = parent_balance - node_balance - 1;
    }
  }
  else
  {
    if ( is_clockwise )
    {
      new_node_balance = std::max( 0, parent_balance + 1 ) + node_balance + 1;
      new_parent_balance = parent_balance + 1;
    }
    else
    {
      new_node_balance = -std::max( 0, -parent_balance + 1 ) + node_balance - 1;
      new_parent_balance = parent_balance - 1;
    }
  }
  
  node_ptr->balance = new_node_balance;
  parent_ptr->balance = new_parent_balance;

  rotate( node_ptr, parent_ptr );
}

} // void ns

template <typename T>
std::string AVLTree<T>::Node::GetLabel() const
{
  return std::to_string( this->GetValue() ) + " ("  + std::to_string( balance ) + ")";
}

template <typename T>
AVLTree<T>::AVLTree() = default;

template <typename T>
AVLTree<T>::~AVLTree() = default;

template <typename T>
typename AVLTree<T>::iterator AVLTree<T>::begin() const
{
  return iterator( root_.get() );
}

template <typename T>
typename AVLTree<T>::iterator AVLTree<T>::end() const
{
  return iterator();
}

template <typename T>
bool AVLTree<T>::Contains( const T &v ) const
{
  const Node *parent;
  auto &cur = get_insertion_position( root_, v, parent );
  return cur != nullptr;
}

template <typename T>
void AVLTree<T>::Insert( const typename Node::value_type &v )
{
  Node *parent;
  std::unique_ptr<Node>& insertion_ptr = get_insertion_position( root_, v , parent );
  if ( insertion_ptr )
    // already inserted
    return;
  insertion_ptr = std::make_unique<Node>( v, parent );

  std::unique_ptr<Node> *cur_ptr = &insertion_ptr;
  while ( parent )
  {
    std::unique_ptr<Node> &parent_ptr = *get_parent_ptr( **cur_ptr, root_ );

    bool is_left = parent_ptr->GetLeft() == *cur_ptr;
    assert( is_left || parent_ptr->GetRight() == *cur_ptr );
    if ( is_left )
      parent_ptr->balance -= 1;
    else
      parent_ptr->balance += 1;

    bool cont = false;
    switch ( std::abs( parent_ptr->balance ) )
    {
    case 0:
      // subtree is now balanced -> no change to balance of super-tree
      break;
    case 1:
      // subtree was balanced before and is now out of balance
      // -> update balance of super-tree
      cont = true;
      break;
    case 2:
      // subtree was already out of balance and has a balance factor of -2 or +2
      // -> need to rotate
      // there are two cases:
      // 1. the higher subtree has the same direction as current
      //    -> a single rotation is needed
      // 2. Otherwise, we rotate the child first, which leads back to case 1.

      // cur_ptr can only have a balance factor or +-1, otherwise we would have
      // rotated in a previous step
      assert( abs((*cur_ptr)->balance) == 1 );
      if ( (*cur_ptr)->balance != parent_ptr->balance / 2 )
      {
        // case 2: rotate child of cur_node to cur_node
        std::unique_ptr<Node> &child_ptr = ((*cur_ptr)->balance == 1)
                                           ? (*cur_ptr)->GetRight()
                                           : (*cur_ptr)->GetLeft();
        rotate_balanced<T>( child_ptr, *cur_ptr );
      }
      // case 1
      rotate_balanced<T>( *cur_ptr, parent_ptr );
      break;
    default:
      assert(false);
    }

    if ( !cont )
      break;

    cur_ptr = &parent_ptr;
    parent = (*cur_ptr)->GetParent();
  }
  assert( is_consistent<T>( *root_ ) );
}

