#include <ostream>
#include <unordered_map>
#include <type_traits>
#include <vector>

template <class Tree>
void print_tree( std::ostream &out, const Tree &tree )
{
  auto end = tree.end();
  for ( auto it = tree.begin(); it != end; ++it )
  {
    for ( size_t i = 0; i < it.depth(); ++i )
      out << "  ";
    out << *it << '\n';
  }
}

template <class Tree>
void print_tree_as_dot( std::ostream &out, const Tree &tree )
{
  out << "digraph tree {\n";
  auto end = tree.end();

  // @todo(ratsch) Tree could provide an interface to extract the value from a node ID
  std::unordered_map<typename Tree::iterator::node_id_type,
                     const typename Tree::iterator::value_type *> node_to_value;
  for ( auto it = tree.begin(); it != end; ++it )
  {
    // add node
    out << "  " << *it << "[ color=" << it.color() << ", label=\"" << it.label() << "\" ];\n";

    auto parent_it = node_to_value.find( it.parent_id() );
    if ( parent_it != node_to_value.end() )
    {
      // add edge if we know the parent
      out << "  " << *parent_it->second << " -> " << *it << ";\n";
    }

    // remember the node ID and its value
    node_to_value.emplace( std::make_pair( it.id(), &(*it) ) );
  }

  out << "}\n" << std::flush;
}
