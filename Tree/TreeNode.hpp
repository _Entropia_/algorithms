#pragma once

#include <memory>

template <typename T, class Derived>
class TreeNode
{
public:
  using self_type = TreeNode<T, Derived>;
  using value_type = T;

  TreeNode( T v, Derived *parent );
  TreeNode( T v, Derived l, Derived r );
  TreeNode( self_type &&other );
  ~TreeNode();

  T & GetValue() { return value_; };
  const T & GetValue() const { return value_; };

  std::unique_ptr<Derived> & GetLeft() { return left_; }
  std::unique_ptr<Derived> & GetRight() { return right_; }
  Derived * GetParent() { return parent_; }
  const std::unique_ptr<Derived> & GetLeft() const { return left_; }
  const std::unique_ptr<Derived> & GetRight() const { return right_; }
  const Derived * GetParent() const { return parent_; }

  void SetParent( Derived *parent ) { parent_ = parent; }

private:
  T value_;
  std::unique_ptr<Derived> left_;
  std::unique_ptr<Derived> right_;
  Derived *parent_ = nullptr;
};

