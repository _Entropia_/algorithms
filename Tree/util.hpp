#include "Tree/TreeNode.hpp"

template <class Node>
void rotate( std::unique_ptr<Node> &n_ptr_ref,
             std::unique_ptr<Node> &p_ptr_ref )
{
  auto n_ptr = &n_ptr_ref;
  auto p_ptr = &p_ptr_ref;
  assert( p_ptr && n_ptr );
  
  Node &p = **p_ptr;
  Node &n = **n_ptr;
  
  bool node_is_left = ( &n == p.GetLeft().get() );
  assert ( node_is_left || &n == p.GetRight().get() );
  
  // node becomes parent
  std::swap( *n_ptr, *p_ptr );
  // p_ptr is a pointer from grandparent and points to n now
  // n_pts is a pointer from p and points to p now
  
  // make n, p point to old node, parent again
  std::swap( n_ptr, p_ptr );
  // n_ptr is a pointer from from grandparent and still points to n
  // p_ptr is a pointer from p and still points to p
  
  // parent becomes other child of node
  std::swap( *p_ptr, node_is_left ? n.GetRight() : n.GetLeft() );
  // p_ptr is a pointer from p and points to one child of n now
  // a child pointer of n now points to p
  
  // restore parent pointers
  n.SetParent( p.GetParent() );
  p.SetParent( &n );
  // the previous child of n is now a child of p
  if ( *p_ptr )
    (*p_ptr)->SetParent( &p );
  
  
}
 
template <class Node>
struct dont_break_search
{
  bool operator()( const Node & ) const
  {
    return false;
  }
};

// @todo(ratsch) how to not duplicate the code?
template <class Node, class F = dont_break_search<Node>>
std::unique_ptr<Node> &
get_insertion_position( std::unique_ptr<Node> &root,
                        const typename Node::value_type &value,
                        Node *&parent,
                        const F &break_search = F() )
{
  parent = nullptr;
  std::unique_ptr<Node> *cur = &root;
  while ( *cur )
  {
    if ( value == (*cur)->GetValue() )
      break;

    if ( break_search( **cur ) )
      break;
    parent = cur->get();
    if ( value < (*cur)->GetValue() )
      cur = &(*cur)->GetLeft();
    else
      cur = &(*cur)->GetRight();
  }
  return *cur;
}
 
template <class Node>
const std::unique_ptr<Node> &
get_insertion_position( const std::unique_ptr<Node> &root,
                        const typename Node::value_type &value,
                        const Node *&parent )
{
  parent = nullptr;
  const std::unique_ptr<Node> *cur = &root;
  while ( *cur )
  {
    if ( value == (*cur)->GetValue() )
      break;

    parent = cur->get();
    if ( value < (*cur)->GetValue() )
      cur = &(*cur)->GetLeft();
    else
      cur = &(*cur)->GetRight();
  }
  return *cur;
}
 
template <class Node>
std::unique_ptr<Node> *
get_parent_ptr( Node &n,
                std::unique_ptr<Node> &root )
{
  if ( n.GetParent() )
  {
    if ( n.GetParent()->GetParent() )
      return n.GetParent() == n.GetParent()->GetParent()->GetLeft().get()
             ? &n.GetParent()->GetParent()->GetLeft()
             : &n.GetParent()->GetParent()->GetRight();
    else
      // there is a parent but no grandparent -> parent pointer is the root
      return &root;
  }
  return nullptr;
}

