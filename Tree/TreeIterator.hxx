#pragma once

#include "Tree/TreeIterator.hpp"

#include <vector>

namespace {
  template<typename C>
  struct has_color {
  private:
    template<typename T>
    static constexpr auto check(T*)
      -> typename std::is_same<decltype( std::declval<T>().GetColor() ), std::string>::type;
    
    template<typename>
    static constexpr std::false_type check(...);
    
    typedef decltype(check<C>(0)) type;
    
  public:
    static constexpr bool value = type::value;
  };
  
  template<typename C>
  struct has_label {
  private:
    template<typename T>
    static constexpr auto check(T*)
      -> typename std::is_same<decltype( std::declval<T>().GetLabel() ), std::string>::type;
    
    template<typename>
    static constexpr std::false_type check(...);
    
    typedef decltype(check<C>(0)) type;
    
  public:
    static constexpr bool value = type::value;
  };
  
  template <class Node>
  typename std::enable_if< has_label<Node>::value, std::string>::type get_label( Node &n )
  {
    return n.GetLabel();
  }
  
  template <class Node>
  typename std::enable_if< !has_label<Node>::value, std::string>::type get_label( Node &n )
  {
    return std::to_string( n.GetValue() );
  }
  
  template <class Node>
  typename std::enable_if< has_color<Node>::value, std::string >::type get_color( Node &n )
  {
    return n.GetColor();
  }
  
  template <class Node>
  typename std::enable_if< !has_color<Node>::value, std::string >::type get_color( Node &n )
  {
    return std::string( "black" );
  }
}

template <class Node>
TreeIterator<Node>::TreeIterator() = default;

template <class Node>
TreeIterator<Node>::TreeIterator( const Node *root )
{
  if ( root )
    stack_.emplace_back( root, 0 );
}

template <class Node>
TreeIterator<Node>::TreeIterator( const TreeIterator<Node> &o ) = default;

template <class Node>
TreeIterator<Node>::~TreeIterator() = default;

template <class Node>
TreeIterator<Node> &
TreeIterator<Node>::operator=( const TreeIterator<Node> &o ) = default;

template <class Node>
const typename TreeIterator<Node>::value_type & TreeIterator<Node>::operator*() const
{
  return stack_.back().first->GetValue();
}

template <class Node>
bool TreeIterator<Node>::operator== ( const TreeIterator<Node> &o ) const
{
  bool is_empty = stack_.empty();
  bool o_is_empty = o.stack_.empty();
  return ( is_empty && o_is_empty ) ||
    ( !is_empty && !o_is_empty && stack_.back() == o.stack_.back() );
}

template <class Node>
bool TreeIterator<Node>::operator!= ( const TreeIterator<Node> &o ) const
{
  return !( *this == o );
}

template <class Node>
typename TreeIterator<Node>::node_id_type TreeIterator<Node>::id() const
{
  return stack_.back().first;
}

template <class Node>
typename TreeIterator<Node>::node_id_type TreeIterator<Node>::parent_id() const
{
  return stack_.back().first->GetParent();
}

template <class Node>
uint64_t TreeIterator<Node>::depth() const
{
  assert( !stack_.empty() );
  return stack_.back().second;
}

template <class Node>
std::string TreeIterator<Node>::color() const
{
  return get_color( *stack_.back().first );
}

template <class Node>
std::string TreeIterator<Node>::label() const
{
  return get_label( *stack_.back().first );
}

template <class Node>
TreeIterator<Node> & TreeIterator<Node>::operator++()
{
  auto &cur = *stack_.back().first;
  auto cur_depth = stack_.back().second;
  stack_.pop_back();

  if ( cur.GetRight() )
    stack_.emplace_back( cur.GetRight().get(), cur_depth + 1 );
  if ( cur.GetLeft() )
    stack_.emplace_back( cur.GetLeft().get(), cur_depth + 1 );

  return *this;
}

