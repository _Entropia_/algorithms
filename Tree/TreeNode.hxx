#pragma once

#include "TreeNode.hpp"

template <typename T, class Derived>
TreeNode<T, Derived>::TreeNode( T v, Derived *parent )
  : value_( v )
  , parent_( parent )
{}

template <typename T, class Derived>
TreeNode<T, Derived>::TreeNode( T v, Derived l, Derived r )
  : value_( v )
  , left_( std::make_unique<Derived>(std::move(l)) )
  , right_( std::make_unique<Derived>(std::move(r)) )
{
  this->l->parent = this;
  this->r->parent = this;
}

template <typename T, class Derived>
TreeNode<T, Derived>::TreeNode( self_type &&other )
  : value_( std::move( other.value_ ) )
  , left_( std::move( other.left_ ) )
  , right_( std::move( other.right_ ) )
  , parent_( other.parent_ )
{
  if ( this->l )
    this->l->parent = this;
  if ( this->r )
    this->r->parent = this;
}

template <typename T, class Derived>
TreeNode<T, Derived>::~TreeNode() = default;

