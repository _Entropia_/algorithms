#pragma once

#include <cinttypes>
#include <memory>
#include <vector>

template <class Node>
class TreeIterator
{
public:
  using value_type = typename Node::value_type;
  using node_id_type = const Node *;

  TreeIterator();
  TreeIterator( const Node *root );
  TreeIterator( const TreeIterator &o );
  ~TreeIterator();

  TreeIterator & operator=( const TreeIterator &o );
  const value_type & operator*() const;
  TreeIterator & operator++();
  bool operator==( const TreeIterator &o ) const;
  bool operator!=( const TreeIterator &o ) const;

  node_id_type id() const;
  node_id_type parent_id() const;
  std::string color() const;
  std::string label() const;

  uint64_t depth() const;

private:
  std::vector<std::pair<const Node *, uint64_t>> stack_;
};

