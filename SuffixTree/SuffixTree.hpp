#include <cassert>
#include <memory>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

class SuffixTree
{
  struct Node {
    char c_;
    std::unordered_map<char, Node *> children_;

    Node( char c ) : c_( c ) {}
  };

  std::vector<std::unique_ptr<Node>> nodes_;
  Node root_;

public:
  SuffixTree( const std::string &s )
    : root_( '\0' )
  {
    std::unordered_set<Node *> root_set;
    root_set.insert( &root_ );
    Node *tail = &root_;
    for ( uint64_t i = 0; i < s.size() + 1; ++i )
    {
      char c = s.c_str()[i];
      nodes_.emplace_back( std::make_unique<Node>( c ) );
      Node &current = *nodes_.back();
      auto res = tail->children_.emplace( current.c_, &current );
      assert( res.second );
      tail = &current;

      std::vector<Node *> new_roots;
      // insert into all root nodes in the set
      for ( auto it = root_set.begin(); it != root_set.end(); )
      {
        auto &root = *it;
        res = root->children_.emplace( current.c_, &current );
        if ( !res.second )
        {
          // character already exists -> insert child into root stack
          new_roots.emplace_back( res.first->second );
        }
        // we always want to insert into the root of the tree -> don't remove it from the set
        if ( root != &root_ )
          it = root_set.erase( it );
        else
          ++it;
      }
      for ( auto root : new_roots )
        root_set.insert( root );
    }
  }
  bool contains( const std::string &s )
  {
    Node *n = &root_;
    for ( char c : s )
    {
      auto it = n->children_.find( c );
      if ( it == n->children_.end() )
        return false;
      n = it->second;
    }
    return true;
  }
};
