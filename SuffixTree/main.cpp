#include <iostream>
#include <random>

#include "SuffixTree.hpp"

using namespace std;

void test_random()
{
  std::mt19937 gen;;
  std::uniform_int_distribution<char> char_dist( 'a', 'c' );
  std::uniform_int_distribution<uint16_t> len_dist( 0, 256 ); 
  
  auto make_string = [&len_dist, &char_dist, &gen]() {
    int16_t len = len_dist( gen );
    std::string s;
    std::generate_n( std::back_inserter( s ), len, std::bind( char_dist, gen ) );
    return s;
  };

  for ( int i = 0; i < 100; ++i )
  {
    auto s = make_string();
    SuffixTree t( s );
    for ( size_t i = 0; i < s.size(); ++i )
    {
      for ( size_t j = i + 1; j < s.size(); ++j )
      {
        std::string sub = s.substr( i, j - i );
        assert( t.contains( sub ) );
      }
    }

    for ( int i = 0; i < 100; ++i )
    {
      auto s2 = make_string();
      bool contains = t.contains( s2 );
      auto pos = s.find( s2 );
      if( ( pos == std::string::npos ) != !contains )
      {
        cout << "FAILED: s=" << s << ", s2=" << s2 << ", contains=" << contains << endl;
        throw false;
      }
    }
  }
}

int main()
{
  SuffixTree t1("bababaabc");
  assert( !t1.contains( "babc" ) );
  test_random();
  return 0;
}
