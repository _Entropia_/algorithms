#include <cassert>
#include <iostream>
#include <functional>
#include <memory>
#include <utility>
#include <vector>

#include "RedBlackTree/RedBlackTree.hpp"
#include "Tree/TreeNode.hxx"
#include "Tree/TreeIterator.hxx"
#include "Tree/util.hpp"

namespace
{
template <typename T>
bool is_consistent( const typename RedBlackTree<T>::Node &root )
{
  int height = -1;
  std::function<bool(const typename RedBlackTree<T>::Node &, int)> count_black;
  count_black = [&height,&count_black]( const typename RedBlackTree<T>::Node &n, int current_height )
  {
    if ( !n.red )
      ++current_height;
    for ( const typename RedBlackTree<T>::Node *child : {n.GetLeft().get(), n.GetRight().get()} )
      if ( child )
      {
        if ( !count_black( *child, current_height ) )
          return false;
      }
      else
      {
        if ( height == -1 )
          height = current_height;
        else if ( current_height != height )
          return false;
      }
    return true;
  };

  std::function<bool(const typename RedBlackTree<T>::Node &, bool)> check_red;
  check_red = [&check_red]( const typename RedBlackTree<T>::Node &n, bool was_red )
  {
    if ( n.red && was_red )
      return false;
    for ( const typename RedBlackTree<T>::Node *child : {n.GetLeft().get(), n.GetRight().get()} )
      if ( child )
        if ( !check_red( *child, n.red ) )
          return false;
    return true;
  };

  std::function<bool(const typename RedBlackTree<T>::Node &)> check_sorted;
  check_sorted = [&check_sorted]( const typename  RedBlackTree<T>::Node &n )
  {
    if ( n.GetLeft() && n.GetLeft()->GetValue() > n.GetValue() )
      return false;
    if ( n.GetRight() && n.GetRight()->GetValue() <= n.GetValue() )
      return false;
    return true;
  };

  return !root.red && count_black( root, 0 ) &&  check_red( root, false ) && check_sorted( root );
}

} // void ns

template <typename T>
std::string RedBlackTree<T>::Node::GetColor() const
{
  return red ? "red" : "black";
}

template <typename T>
RedBlackTree<T>::RedBlackTree() = default;

template <typename T>
RedBlackTree<T>::~RedBlackTree() = default;

template <typename T>
typename RedBlackTree<T>::iterator RedBlackTree<T>::begin() const
{
  return iterator( root_.get() );
}

template <typename T>
typename RedBlackTree<T>::iterator RedBlackTree<T>::end() const
{
  return iterator();
}

template <typename T>
bool RedBlackTree<T>::Contains( const T &v ) const
{
  const Node *parent;
  auto &cur = get_insertion_position( root_, v, parent );
  return cur != nullptr;
}

template <typename T>
void RedBlackTree<T>::Insert( const typename Node::value_type &v )
{
  std::unique_ptr<Node> *n_ptr = &root_;
  std::unique_ptr<Node> *p_ptr = nullptr;
  std::unique_ptr<Node> *g_ptr = nullptr;

  while (*n_ptr)
  {
    g_ptr = p_ptr;
    p_ptr = n_ptr;
    Node &cur = **n_ptr;
    if ( v < cur.GetValue() )
      n_ptr = &cur.GetLeft();
    else if ( v > cur.GetValue() )
      n_ptr = &cur.GetRight();
    else
      // value already exists
      return;
  }
  
  // insert new node as red child
  *n_ptr = std::make_unique<Node>( v, !p_ptr ? nullptr : p_ptr->get() );
  (*n_ptr)->red = true;

  while ( true )
  {
    // find parent and grandparent
    if ( !p_ptr )
    {
      p_ptr = get_parent_ptr( **n_ptr, root_ );
    }
    if ( p_ptr && !g_ptr )
    {
      g_ptr = get_parent_ptr( **p_ptr, root_ );
    }
    
    if ( !p_ptr )
    {
      // case 1: there is no root -> insert root
      // newly inserted root should be black
      (*n_ptr)->red = false;
      // done inserting
      break;
    }
    else if ( !(*p_ptr)->red )
    {
      // case 2: the parent is black
      // nothing to do (keep it red)
      // done inserting
      break;
    }
    else
    {
      // there is a parent and it is red
      // -> there is a grandparent which is black
      assert( !(*g_ptr)->red );
      
      bool node_is_left = ( n_ptr == &(*p_ptr)->GetLeft() );
      bool parent_is_left = ( p_ptr->get() == (*g_ptr)->GetLeft().get() );
      
      // determine uncle
      
      Node *p = p_ptr->get();
      Node *g = g_ptr->get();
      Node *n = n_ptr->get();
      Node *u = (parent_is_left ? (*g_ptr)->GetRight() : (*g_ptr)->GetLeft()).get();
      
      if ( !u || !u->red )
      {
        // uncle is black -> case 4 or 5
        if ( node_is_left != parent_is_left )
        {
          // inner direction -> case 4
          // rotate node to parent, then continue with case 5
          rotate( *n_ptr, *p_ptr );
          // p is the new node now, and it's in outer direction now
          n_ptr = node_is_left ? &(*p_ptr)->GetRight() : &(*p_ptr)->GetLeft();
          assert( n == p_ptr->get() );
          assert( p == n_ptr->get() );
          std::swap( p, n );
          // continue with case 5
        }
        
        // outer direction -> case 5
        // rotate parent to grandparent
        rotate( *p_ptr, *g_ptr );
        
        // parent becomes black since it takes the role of grandparent
        p->red = false;
        // grandparent becomes red since it's under parent now
        g->red = true;
        
        // done inserting
        break;
      }
      else
      {
        // uncle is red -> case 3
        // toggle coloring and shift the problem up by two levels
        p->red = false;
        g->red = true;
        u->red = false;
        
        n_ptr = g_ptr;
        p_ptr = g_ptr = nullptr;
        // continue with n_ptr pointing to the grandparent
      }
    }
  }

  assert( is_consistent<int>( *root_ ) );
}

