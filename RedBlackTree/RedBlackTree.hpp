#include <iostream>
#include <memory>

#include "Tree/TreeNode.hpp"
#include "Tree/TreeIterator.hpp"

template <typename T>
class RedBlackTree
{
public:
  class Node : public TreeNode<T, RedBlackTree<T>::Node>
  {
  public:
    using TreeNode<T, Node>::TreeNode;
    bool red = false;
    
    std::string GetColor() const;
  };

  using iterator = TreeIterator<Node>;

  RedBlackTree();
  ~RedBlackTree();

  iterator begin() const;
  iterator end() const;
  
  // @todo(ratsch) add inorder iterator, return that
  bool Contains( const T &v ) const;

  void Insert( const typename Node::value_type &v );

private:
  std::unique_ptr<Node> root_;
};

