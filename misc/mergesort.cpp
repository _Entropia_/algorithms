#include <iostream>
#include <vector>
using namespace std;

template <typename T>
void sort( vector<T> &v, size_t begin, size_t end )
{
	if ( begin + 1 >= end )
		return;
	sort( v, begin, (begin + end) / 2 );
	sort( v, (begin + end) / 2, end );
	vector<T> tmp;
	auto it1 = v.begin() + begin;
	auto it2 = v.begin() + (begin + end) / 2;
	const auto it1end = it2;
	const auto it2end = v.begin() + end;
	
	while ( it1 != it1end && it2 != it2end )
	{
		if ( *it1 < *it2 )
		{
			tmp.push_back( *it1 );
			++it1;
		}
		else
		{
			tmp.push_back( *it2 );
			++it2;
		}
	}
	while ( it1 != it1end )
		tmp.push_back( *(it1++) );
	while ( it2 != it2end )
		tmp.push_back( *(it2++) );

	auto it_out = v.begin() + begin;
	for ( auto it_in = tmp.begin(); it_in != tmp.end(); ++it_in, ++it_out )
		*it_out = *it_in;
}
int main()
{
	vector<int> v{2, 4, 87, 9, 5, 2, 1, 3,5 ,6 ,4, 3, 345, 54, 6, 767,5 };
	sort( v, 0, v.size() );
	for ( auto &val : v )
	{
		cout << val << ", ";
	}
	cout << endl;
	return 0;
}
