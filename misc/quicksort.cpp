#include <utility>
#include <vector>
#include <iostream>
using namespace std;

template <typename T>
size_t partition( vector<T> &v, size_t begin, size_t end )
{
	size_t p_i = begin;
	for ( size_t i = begin + 1; i < end; )
	{
		if ( v[i] <= v[p_i] )
		{
			swap( v[p_i], v[i] );
			p_i = i;
			++i;
		}
		else if ( v[i] > v[p_i] )
		{
			swap( v[i], v[ --end ] );
		}
	}
	return p_i;
}


template <typename T>
void sort(vector<T> &v, size_t begin, size_t end)
{
	if ( begin + 1 >= end )
		return;
	auto p_i = partition( v, begin, end );
	sort( v, begin, p_i );
	sort( v, p_i + 1, end );
}

int main()
{
	vector<int> v{2, 4, 87, 9, 5, 2, 1, 3,5 ,6 ,4, 3, 345, 54, 6, 767,5 };
	sort( v, 0, v.size() );
	for ( auto &val : v )
	{
		cout << val << ", ";
	}
	cout << endl;
	return 0;
}
