#include <cassert>
#include <iostream>
#include <functional>
#include <utility>
#include <vector>

using namespace std;

template< typename T >
class HashSet
{
public:
	HashSet()
{
		buckets_.resize(128);
}

void add( const T &val )
{
	hash<T> h;
	auto hash_val = h( val );
	size_t start_idx = hash_val % buckets_.size();
	// linear probing
	bool first = true;
	for ( size_t i = start_idx; first || i != start_idx;
            i = ( i + 1 ) % buckets_.size() )
	{
		first = false;
		if ( !buckets_[i].first )
		{
			buckets_[i] = make_pair( true, val );
			++size_;
			break;
		}
		else if ( buckets_[i].second == val )
			// already contains this element
			return;
	}
	if ( size_ * 2 > buckets_.size() )
		buckets_.resize( buckets_.size() * 2 );
}


bool contains( const T &val )
{
	hash<T> h;
	auto hash_val = h( val );
	size_t start_idx = hash_val % buckets_.size();
	// linear probing
	bool first = true;
	for ( size_t i = start_idx; first || i != start_idx;
            i = ( i + 1 ) % buckets_.size() )
	{
		first = false;
		if ( !buckets_[i].first )
			return false;
		else if ( buckets_[i].second == val )
			return true;
	}
	assert(false);
	return false; // cannot happen - there are always empty buckets
}


size_t size() const
{
	return size_;
}


private:
	vector< pair<bool, T> > buckets_;
	size_t size_ = 0;
};

int main()
{
	HashSet<int> map;
	map.add(1);
	map.add(2);
	map.add(2);
	cout << map.size() << " " << map.contains(2) << " " << map.contains(4) << endl;
	return 0;
}
