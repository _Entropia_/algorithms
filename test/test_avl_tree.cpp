#include <cassert>
#include <iostream>
#include <fstream>
#include <random>
#include <unordered_set>

#include "AVLTree/AVLTree.hxx"
#include "util/tree_printer.hpp"

using std::cin;
using std::cout;
using std::endl;

void print_dot( const AVLTree<int> &tree )
{
  std::ofstream f_out( "tree.dot" );
  print_tree_as_dot(f_out, tree);
  f_out.close();
}

void test_ascending()
{
  AVLTree<int> tree;
  for ( int i = -100; i < 100; ++i )
  {
    tree.Insert( i );
  }
  
  for ( int i = -100; i < 100; ++i )
  {
    if ( !tree.Contains( i ) )
      throw std::runtime_error("value not in tree");
  }
}

void test_decending()
{
  AVLTree<int> tree;
  for ( int i = 100; i > -100; --i )
  {
    tree.Insert( i );
  }
  
  for ( int i = 100; i > 100; --i )
  {
    if ( !tree.Contains( i ) )
      throw std::runtime_error("value not in tree");
  }
}

void test_random()
{
  std::unordered_set<int> reference;
  AVLTree<int> tree;
  
  std::mt19937 random;
  std::uniform_int_distribution<int> dist( -100000, 100000 );
  for ( int i = 0; i < 10000; ++i )
  {
    int val = dist( random );
    reference.insert( val );
    tree.Insert( val );
  }
  for ( int val : reference )
  {
    if ( !tree.Contains( val ) )
      throw std::runtime_error("value not in tree");
  }
  
  for ( int i = 0; i < 10000; ++i )
  {
    int val = dist( random );
    auto it = reference.find( val );
    bool contains = tree.Contains( val );
    if( contains != ( it != reference.end() ) )
      throw std::runtime_error("inconsistency between tree and reference");
  }
  
  std::unordered_set<int> all_values;
  for ( auto it = tree.begin(); it != tree.end(); ++it )
  {
    all_values.insert( *it );
  }
  if( all_values != reference )
    throw std::runtime_error("all values in tree not equal to reference");
}

int main()
{
  test_ascending();
  test_decending();
  test_random();
  return 0;
  
  /*AVLTree<int> tree;
  while ( true )
  {
    int i;
    cin >> i;
    
    tree.Insert(i);
    print_dot( tree );
  }
  return 0;*/
}
