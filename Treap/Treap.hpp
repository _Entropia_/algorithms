#include <cinttypes>
#include <iostream>
#include <memory>

#include "Tree/TreeNode.hpp"
#include "Tree/TreeIterator.hpp"

template <typename T>
class Treap
{
public:
  // @todo(ratsch) does it need to be public?
  class Node : public TreeNode<T, Treap<T>::Node>
  {
  public:
    using TreeNode<T, Node>::TreeNode;
    int64_t priority = 0;
    
    std::string GetLabel() const;
  };

  using iterator = TreeIterator<Node>;

  Treap();
  ~Treap();

  iterator begin() const;
  iterator end() const;
  
  // @todo(ratsch) add inorder iterator, return that
  bool Contains( const T &v ) const;

  void Insert( const typename Node::value_type &v );

private:
  std::unique_ptr<Node> root_;
};

