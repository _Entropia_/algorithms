#include <cassert>
#include <cmath>
#include <iostream>
#include <functional>
#include <memory>
#include <utility>
#include <random>
#include <stdexcept>
#include <vector>

#include "Treap/Treap.hpp"
#include "Tree/TreeNode.hxx"
#include "Tree/TreeIterator.hxx"
#include "Tree/util.hpp"

namespace
{

template<typename T>
using node_t = typename Treap<T>::Node;

template <typename T>
using nodeptr_t = std::unique_ptr<node_t<T>>;

template <typename T>
bool is_consistent( const typename Treap<T>::Node &root )
{
  std::function<bool(const typename Treap<T>::Node &)> check_sorted;
  check_sorted = [&check_sorted]( const typename Treap<T>::Node &n )
  {
    if ( n.GetLeft() && n.GetLeft()->GetValue() > n.GetValue() )
      return false;
    if ( n.GetRight() && n.GetRight()->GetValue() <= n.GetValue() )
      return false;
    return true;
  };
  
  std::function<bool(const typename Treap<T>::Node &)> check_heapified;
  check_heapified = [&check_heapified]( const typename Treap<T>::Node &n )
  {
    for ( const auto *child : { &n.GetLeft(), &n.GetRight() } )
    {
      if ( *child && ( (*child)->priority >= n.priority || !check_heapified( **child ) ) )
        return false;
    }
    return true;
  };

  return check_sorted( root ) && check_heapified( root );
}

template <typename T>
void split( nodeptr_t<T> &root_ref, T value, nodeptr_t<T> &l_out, nodeptr_t<T> &r_out )
{
  if ( !root_ref )
    return;

  // left : values <= value
  node_t<T> &root = *root_ref;
  if ( root.GetValue() <= value )
  {
    // root belongs to left subtree
    nodeptr_t<T> l_sub, r_sub;
    split( root.GetRight(), value, l_sub, r_sub );

    root.GetRight() = std::move( l_sub );
    if ( root.GetRight() )
      root.GetRight()->SetParent( &root );

    l_out = std::move( root_ref );
    r_out = std::move( r_sub );
  }
  else
  {
    // root belongs to right subtree
    nodeptr_t<T> l_sub, r_sub;
    split( root.GetLeft(), value, l_sub, r_sub );

    root.GetLeft() = std::move( r_sub );
    if ( root.GetLeft() )
      root.GetLeft()->SetParent( &root );

    l_out = std::move( l_sub );
    r_out = std::move( root_ref );
  }
  if ( l_out )
    l_out->SetParent( nullptr );
  if ( r_out )
    r_out->SetParent( nullptr );
}

template <typename T>
void join( nodeptr_t<T> &t1, nodeptr_t<T> &t2, nodeptr_t<T> &t_out )
{
  if ( !t1 )
  {
    t_out = std::move( t2 );
    return;
  }
  else if ( !t2 )
  {
    t_out = std::move( t1 );
    return;
  }

  if ( t1->GetValue() > t2->GetValue() )
    std::swap( t1, t1 );

  if ( t1->priority > t2->priority )
  {
    // root of t1 will be new root, t2 + right subtree of t1 will be new right subtree
    nodeptr_t<T> r_sub;
    join( t1->GetRight(), t2, r_sub );
    t1->GetRight() = std::move( r_sub );
    t1->GetRight()->SetParent( t1.get() );
    t_out = std::move( t1 );
  }
  else
  {
    // root of t2 will be new root, t1 + left subtree of t2 will be new left subtree
    nodeptr_t<T> l_sub;
    join( t2->GetLeft(), t1, l_sub );
    t2->GetLeft() = std::move( l_sub );
    t2->GetLeft()->SetParent( t2.get() );
    t_out = std::move( t2 );
  }
}
  
} // void ns

template <typename T>
std::string Treap<T>::Node::GetLabel() const
{
  return std::to_string( this->GetValue() ) + " ("  + std::to_string( priority ) + ")";
}

template <typename T>
Treap<T>::Treap() = default;

template <typename T>
Treap<T>::~Treap() = default;

template <typename T>
typename Treap<T>::iterator Treap<T>::begin() const
{
  return iterator( root_.get() );
}

template <typename T>
typename Treap<T>::iterator Treap<T>::end() const
{
  return iterator();
}

template <typename T>
bool Treap<T>::Contains( const T &v ) const
{
  const Node *parent;
  auto &cur = get_insertion_position( root_, v, parent );
  return cur != nullptr;
}

template <typename T>
void Treap<T>::Insert( const typename Node::value_type &v )
{
  static std::mt19937 rand;
  static std::uniform_int_distribution<int64_t> dist( std::numeric_limits<int64_t>::min() );

  int64_t new_priority = dist( rand );
  Node *parent;

  // check that we don't already have this value
  if ( get_insertion_position( root_, v, parent ) )
    // we have it already
    return;

  std::unique_ptr<Node>& insertion_ptr = get_insertion_position( root_, v , parent, [new_priority]( const Node &n ) {
    return n.priority < new_priority;
  } );

  auto new_node = std::make_unique<Node>( v, parent );
  new_node->priority = new_priority;
  if ( !insertion_ptr )
  {
    // simply insert
    insertion_ptr = std::move( new_node );
  }
  else
  {
    // need to split insertion position by inserted value
    std::unique_ptr<Node> l, r;
    split( insertion_ptr, v, l, r );
    insertion_ptr = std::move( new_node );
    if ( l )
      l->SetParent( insertion_ptr.get() );
    if ( r )
      r->SetParent( insertion_ptr.get() );
    insertion_ptr->GetLeft() = std::move( l );
    insertion_ptr->GetRight() = std::move( r );
  }

  assert( is_consistent<T>( *root_ ) );
}

